package com.company.command;

import com.company.toyrobot.command.GenericCommand;
import org.junit.jupiter.api.Test;

import static com.company.command.CommandResult.CompletionStatus.FAILED;
import static com.company.command.CommandResult.CompletionStatus.SUCCESS;
import static org.assertj.core.api.Assertions.assertThat;

class CommandResultTest {

    private Command genericCommand = new GenericCommand(1);

    @Test
    void successShouldReturnCommandResultWithSuccessAndNullMessage() {
        assertCommandResult(CommandResult.success(genericCommand),
                SUCCESS, null, genericCommand, null);
    }

    @Test
    void successWithMessageShouldReturnCommandResultWithSuccessAndMessage() {
        assertCommandResult(CommandResult.success("Hello world!", genericCommand),
                SUCCESS, "Hello world!", genericCommand, null);
    }

    @Test
    void failedShouldReturnCommandResultWithFailedAndMessage() {
        assertCommandResult(CommandResult.failed("Hello world!", genericCommand),
                FAILED, "Hello world!", genericCommand, null);
    }

    @Test
    void failedShouldReturnCommandResultWithFailedAndMessageAndException() {
        Exception exception = new Exception("Something happened");
        assertCommandResult(CommandResult.failed("Hello world!", genericCommand, exception),
                FAILED, "Hello world!", genericCommand, exception);
    }

    private void assertCommandResult(CommandResult commandResult, CommandResult.CompletionStatus completionStatus, String message, Command command, Exception exception) {
        assertThat(commandResult.getCompletionStatus()).isEqualTo(completionStatus);
        assertThat(commandResult.getMessage()).isEqualTo(message);
        assertThat(commandResult.getCommand()).isEqualTo(command);
        assertThat(commandResult.getException()).isEqualTo(exception);
    }
}