package com.company.command.processor;

import com.company.command.Command;
import com.company.command.CommandResult;
import com.company.command.configuration.CommandErrorHandlerConfiguration;
import com.company.toyrobot.command.GenericCommand;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

class CommandErrorHandlerTest {

    private Command genericCommand = new GenericCommand(123);
    private CommandErrorHandlerConfiguration commandErrorHandlerConfiguration = new CommandErrorHandlerConfiguration(false);
    private Writer writer = new StringWriter();
    private CommandErrorHandler commandErrorHandler = new CommandErrorHandler(commandErrorHandlerConfiguration, writer);

    @Test
    void handleErrorShouldWriteTheCommandAndReasonAndNotThrowExceptionWhenFailFastIsFalse() {
        commandErrorHandler.handleError(CommandResult.failed("Failed for such and such reason", genericCommand));
        assertThat(writer).hasToString("GenericCommand[forEqualsCheck=123]: Failed for such and such reason" + System.lineSeparator());
    }

    @Test
    void handleErrorShouldNotWriteAnythingIfCommandResultIsSuccessful() {
        commandErrorHandler.handleError(CommandResult.success("Works!", genericCommand));
        assertThat(writer).hasToString("");
    }

    @Test
    void handleErrorShouldWriteTheCommandAndReasonAndThrowCommandFailedExceptionIfFailFastIsTrue() {
        CommandErrorHandler commandErrorHandler = new CommandErrorHandler(new CommandErrorHandlerConfiguration(true), writer);

        CommandFailedException commandFailedException = assertThrows(CommandFailedException.class,
                () -> commandErrorHandler.handleError(CommandResult.failed("Failed for some reason", genericCommand)));
        assertThat(commandFailedException).hasMessage("Failed to process command 'GenericCommand[forEqualsCheck=123]' due to: Failed for some reason");

        assertThat(writer).hasToString("GenericCommand[forEqualsCheck=123]: Failed for some reason" + System.lineSeparator());
    }

    @Test
    void handleErrorShouldNotThrowExceptionWhenWritingFails() throws IOException {
        writer = mock(Writer.class);
        commandErrorHandler = new CommandErrorHandler(commandErrorHandlerConfiguration, writer);
        doThrow(new IOException("Could not write because of IO error")).when(writer).write(anyString());

        commandErrorHandler.handleError(CommandResult.failed("Failed for such and such reason", genericCommand));
    }
}