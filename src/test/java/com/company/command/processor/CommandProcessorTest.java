package com.company.command.processor;

import com.company.command.Command;
import com.company.command.CommandResult;
import com.company.domain.Position;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.junit.jupiter.api.Test;

import static com.company.domain.Direction.NORTH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class CommandProcessorTest {

    private CommandErrorHandler commandErrorHandler = mock(CommandErrorHandler.class);
    private CommandProcessor commandProcessor = new CommandProcessor(commandErrorHandler);
    private Table table = new Table(5, 5);
    private ToyRobot toyRobot = new ToyRobot(new Position(0, 0), NORTH);

    @Test
    void processCommandShouldReturnCommandResultAndNotInvokeCommandErrorHandlerWhenSuccess() {
        Command command = mock(Command.class);
        CommandResult commandResult = CommandResult.success(command);
        when(command.execute(table, toyRobot)).thenReturn(commandResult);

        assertThat(commandProcessor.processCommand(table, toyRobot, command)).isEqualTo(commandResult);

        verify(commandErrorHandler, never()).handleError(commandResult);
    }

    @Test
    void processCommandShouldReturnCommandResultAndInvokeCommandErrorHandlerWhenFailed() {
        Command command = mock(Command.class);
        CommandResult commandResult = CommandResult.failed("Failed because of such and such", command);
        when(command.execute(table, toyRobot)).thenReturn(commandResult);

        assertThat(commandProcessor.processCommand(table, toyRobot, command)).isEqualTo(commandResult);

        verify(commandErrorHandler, times(1)).handleError(commandResult);
    }
}