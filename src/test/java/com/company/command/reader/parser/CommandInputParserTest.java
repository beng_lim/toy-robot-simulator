package com.company.command.reader.parser;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CommandInputParserTest {

    private CommandInputParser commandInputParser = new CommandInputParser();

    @Test
    void parseRawCommandShouldThrowNullPointerExceptionWhenNullCommandStringProvided() {
        assertThrows(NullPointerException.class, () -> commandInputParser.parseRawCommand(null));
    }

    @Test
    void parseRawCommandShouldThrowCommandParsingExceptionWhenEmptyCommandStringProvided() {
        assertThrows(CommandParsingException.class, () -> commandInputParser.parseRawCommand(""));
    }

    @Test
    void parseRawCommandShouldReturnCommandInputWithEmptyArgumentsWhenOnlyNameProvided() {
        assertThat(commandInputParser.parseRawCommand("MOVE")).isEqualTo(new CommandInput("MOVE", emptyList()));
    }

    @Test
    void parseRawCommandShouldReturnCommandInputWithArgumentsWhenNameAndArgumentsProvided() {
        assertThat(commandInputParser.parseRawCommand("MOVE arg1,arg2,arg3")).isEqualTo(new CommandInput("MOVE", Arrays.asList(
                "arg1", "arg2", "arg3"
        )));
    }

    @Test
    void parseRawCommandShouldReturnCommandInputWithArgumentsWhenSomeArgumentsAreEmpty() {
        assertThat(commandInputParser.parseRawCommand("MOVE arg1,,,arg4")).isEqualTo(new CommandInput("MOVE", Arrays.asList(
                "arg1", "", "", "arg4"
        )));
    }

    @Test
    void parseRawCommandShouldIgnoreAdditionalCommandArgumentsAfterSecondIndex() {
        assertThat(commandInputParser.parseRawCommand("MOVE arg1 hello world")).isEqualTo(new CommandInput("MOVE", Arrays.asList(
                "arg1"
        )));
    }
}