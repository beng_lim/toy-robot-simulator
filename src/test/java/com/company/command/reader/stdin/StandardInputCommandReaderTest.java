package com.company.command.reader.stdin;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class StandardInputCommandReaderTest {

    @Test
    void readShouldThrowUnsupportedOperationException() {
        StandardInputCommandReader commandReader = new StandardInputCommandReader();
        assertThrows(UnsupportedOperationException.class, commandReader::read);
    }

    @Test
    void closeShouldThrowUnsupportedOperationException() {
        StandardInputCommandReader commandReader = new StandardInputCommandReader();
        assertThrows(UnsupportedOperationException.class, commandReader::close);
    }
}