package com.company.command.reader.stdin;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class StandardInputCommandReaderFactoryTest {

    private StandardInputCommandReaderFactory standardInputCommandReaderFactory = new StandardInputCommandReaderFactory();

    @Test
    void fromArgumentsShouldReturnAStandardInputCommandReader() {
        assertThat(standardInputCommandReaderFactory.fromArguments(new String[]{})).isOfAnyClassIn(
                StandardInputCommandReader.class
        );
    }
}