package com.company.command.reader.file;

import com.company.command.Command;
import com.company.command.reader.factory.CommandFactory;
import com.company.toyrobot.command.GenericCommand;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FileInputCommandReaderTest {

    private CommandFactory commandFactory = mock(CommandFactory.class);

    @Test
    void constructorShouldThrowFileNotFoundWhenInvalidFileProvided() {
        assertThrows(FileNotFoundException.class, () -> new FileInputCommandReader(commandFactory, "some_random_file_that_shouldnt_exist"));
    }

    @Test
    void readShouldReturnAStreamWithExpectedCommands() throws IOException {
        GenericCommand command1 = new GenericCommand(1);
        GenericCommand command2 = new GenericCommand(2);
        GenericCommand command3 = new GenericCommand(3);

        when(commandFactory.createCommand("PLACE 0,0,NORTH")).thenReturn(command1);
        when(commandFactory.createCommand("MOVE")).thenReturn(command2);
        when(commandFactory.createCommand("REPORT")).thenReturn(command3);

        List<Command> commands;

        try (FileInputCommandReader fileInputCommandReader = new FileInputCommandReader(commandFactory, "src/test/resources/input_files/sample_a.txt")) {
            commands = fileInputCommandReader.read().collect(Collectors.toList());
        }

        assertThat(commands).containsExactly(command1, command2, command3);
    }

    @Test
    void readShouldReturnStreamWithNoElementsWhenFileIsEmpty() throws IOException {
        List<Command> commands;

        try (FileInputCommandReader fileInputCommandReader = new FileInputCommandReader(commandFactory, "src/test/resources/input_files/empty.txt")) {
            commands = fileInputCommandReader.read().collect(Collectors.toList());
        }

        assertThat(commands).isEmpty();
    }

    @Test
    void readShouldReturnAStreamIgnoringAllEmptyLines() throws IOException {
        List<Command> commands;

        try (FileInputCommandReader fileInputCommandReader = new FileInputCommandReader(commandFactory, "src/test/resources/input_files/empty_lines.txt")) {
            commands = fileInputCommandReader.read().collect(Collectors.toList());
        }

        assertThat(commands).isEmpty();
    }
}