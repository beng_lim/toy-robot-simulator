package com.company.command.reader.file;

import com.company.command.reader.CommandReader;
import com.company.command.reader.factory.CommandFactory;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

class FileInputCommandReaderFactoryTest {

    private CommandFactory commandFactory = mock(CommandFactory.class);
    private FileInputCommandReaderFactory fileInputCommandReaderFactory = new FileInputCommandReaderFactory(commandFactory);

    @Test
    void fromArgumentsShouldThrowIllegalArgumentsExceptionWhenLessThanExpectedNumberOfArgumentsProvided() {
        assertThrows(IllegalArgumentException.class, () -> fileInputCommandReaderFactory.fromArguments(new String[]{}));
    }

    @Test
    void constructorShouldReturnExpectedCommandLineArgumentsWhenAllArgumentsProvided() {
        String filePath = "src/test/resources/input_files/sample_a.txt";

        CommandReader commandReader = fileInputCommandReaderFactory.fromArguments(new String[]{filePath});
        assertThat(commandReader).isOfAnyClassIn(FileInputCommandReader.class);

        assertThat(((FileInputCommandReader) commandReader).getFilePath()).isEqualTo(Paths.get(filePath));
    }

    @Test
    void constructorShouldReturnExpectedCommandLineArgumentsWhenMoreArgumentsProvidedThanExpected() {
        String filePath = "src/test/resources/input_files/sample_a.txt";

        CommandReader commandReader = fileInputCommandReaderFactory.fromArguments(new String[]{filePath, "another_arg"});
        assertThat(commandReader).isOfAnyClassIn(FileInputCommandReader.class);

        assertThat(((FileInputCommandReader) commandReader).getFilePath()).isEqualTo(Paths.get(filePath));
    }
}