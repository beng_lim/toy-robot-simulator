package com.company.command.reader.configuration;

import com.company.command.Command;
import com.company.command.reader.parser.CommandInput;
import com.company.command.reader.parser.CommandParser;
import com.company.toyrobot.command.parser.LeftCommandParser;
import com.company.toyrobot.command.parser.PlaceCommandParser;
import com.company.toyrobot.command.parser.RightCommandParser;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

class CommandConfigurationTest {

    private CommandConfiguration commandConfiguration = new CommandConfiguration();

    @Test
    void commandParsersShouldReturnMapOfCommandParsersWhereKeyIsCommandName() {
        CommandParser leftCommandParser = new LeftCommandParser();
        CommandParser rightCommandParser = new RightCommandParser();
        CommandParser placeCommandParser = new PlaceCommandParser();
        List<CommandParser> commandParsers = Arrays.asList(leftCommandParser, rightCommandParser, placeCommandParser);

        assertThat(commandConfiguration.commandParsers(commandParsers)).containsExactly(
                entry("LEFT", leftCommandParser),
                entry("RIGHT", rightCommandParser),
                entry("PLACE", placeCommandParser)
        );
    }

    @Test
    void commandParsersShouldReturnMapOfCommandParsersWhereKeyIsUpperCase() {
        CommandParser anotherParser = new AnotherParser();
        List<CommandParser> commandParsers = Collections.singletonList(anotherParser);

        assertThat(commandConfiguration.commandParsers(commandParsers)).containsExactly(entry("COMMAND_ABC_123", anotherParser));
    }

    private class AnotherParser implements CommandParser {

        @Override
        public String getCommandName() {
            return "command_abc_123";
        }

        @Override
        public Command parse(CommandInput commandInput) {
            return null;
        }
    }
}