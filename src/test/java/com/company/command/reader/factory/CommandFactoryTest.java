package com.company.command.reader.factory;

import com.company.command.reader.parser.CommandInput;
import com.company.command.reader.parser.CommandInputParser;
import com.company.command.reader.parser.CommandParser;
import com.company.toyrobot.command.GenericCommand;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CommandFactoryTest {

    private CommandInputParser commandInputParser = new CommandInputParser();
    private Map<String, CommandParser> commandParsers = new HashMap<String, CommandParser>() {{
        put("LEFT", mock(CommandParser.class));
        put("RIGHT", mock(CommandParser.class));
    }};
    private CommandFactory commandFactory = new CommandFactory(commandInputParser, commandParsers);

    @Test
    void createCommandShouldThrowNullPointerExceptionWhenNullRawCommandIsProvided() {
        assertThrows(NullPointerException.class, () -> commandFactory.createCommand(null));
    }

    @Test
    void createCommandShouldThrowIllegalArgumentExceptionWhenEmptyRawCommandIsProvided() {
        assertThrows(IllegalArgumentException.class, () -> commandFactory.createCommand(""));
    }

    @Test
    void createCommandShouldUseTheLeftParserToCreateACommandWhenTheCommandNameIsUpperCaseLeft() {
        String rawCommand = "LEFT 123";
        GenericCommand command = new GenericCommand(123);
        when(commandParsers.get("LEFT").parse(new CommandInput("LEFT", Collections.singletonList("123")))).thenReturn(command);

        assertThat(commandFactory.createCommand(rawCommand)).isEqualTo(command);
    }

    @Test
    void createCommandShouldUseTheLeftParserToCreateACommandWhenTheCommandNameIsLowerCaseLeft() {
        String rawCommand = "left 345";
        GenericCommand command = new GenericCommand(345);
        when(commandParsers.get("LEFT").parse(new CommandInput("LEFT", Collections.singletonList("345")))).thenReturn(command);

        assertThat(commandFactory.createCommand(rawCommand)).isEqualTo(command);
    }

    @Test
    void createCommandShouldUseTheLeftParserToCreateACommandWhenTheCommandNameIsMixedCaseLeft() {
        String rawCommand = "LeFt 456";
        GenericCommand command = new GenericCommand(456);
        when(commandParsers.get("LEFT").parse(new CommandInput("LEFT", Collections.singletonList("456")))).thenReturn(command);

        assertThat(commandFactory.createCommand(rawCommand)).isEqualTo(command);
    }

    @Test
    void createCommandShouldUseTheRightParserToCreateACommandWhenTheCommandNameIsUpperCaseRight() {
        String rawCommand = "RIGHT 111";
        GenericCommand command = new GenericCommand(111);
        when(commandParsers.get("RIGHT").parse(new CommandInput("RIGHT", Collections.singletonList("111")))).thenReturn(command);

        assertThat(commandFactory.createCommand(rawCommand)).isEqualTo(command);
    }

    @Test
    void createCommandShouldThrowUnsupportedOperationExceptionWhenTheCommandNameDoesNotExist() {
        String rawCommand = "righ 345";

        assertThrows(UnsupportedOperationException.class, () -> commandFactory.createCommand(rawCommand));
    }
}