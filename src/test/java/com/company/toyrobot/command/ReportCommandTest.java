package com.company.toyrobot.command;

import com.company.command.CommandResult;
import com.company.domain.Position;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import static com.company.domain.Direction.NORTH;
import static com.company.domain.Direction.SOUTH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

class ReportCommandTest {

    private Table table = new Table(5, 5);
    private ToyRobot toyRobot = new ToyRobot(new Position(2, 3), SOUTH);
    private Writer writer = new StringWriter();
    private ReportCommand reportCommand = new ReportCommand(writer);

    @BeforeEach
    void setUp() {
        table.setToyRobot(toyRobot);
    }

    @Test
    void executeShouldNotReportOnTheRobotsPositionWhenRobotIsNotPlacedOnTable() {
        Table table = new Table(5, 5);
        ToyRobot toyRobot = new ToyRobot(new Position(0, 0), NORTH);

        assertThat(reportCommand.execute(table, toyRobot)).isEqualTo(CommandResult.failed("Toy robot has not been placed on table yet.", reportCommand));
    }

    @Test
    void executeShouldReportToyRobotPositionAndDirection() {
        assertThat(reportCommand.execute(table, toyRobot)).isEqualTo(CommandResult.success(reportCommand));
        assertThat(writer).hasToString("Output: 2,3,SOUTH" + System.lineSeparator());
    }

    @Test
    void executeShouldReturnFailedWhenReportingFails() throws IOException {
        writer = mock(Writer.class);
        reportCommand = new ReportCommand(writer);

        IOException ioException = new IOException("Could not write");
        doThrow(ioException).when(writer).write(anyString());

        assertThat(reportCommand.execute(table, toyRobot)).isEqualTo(CommandResult.failed(
                "Could not report position due to IO error.", reportCommand, ioException
        ));
    }
}