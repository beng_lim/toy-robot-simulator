package com.company.toyrobot.command;

import com.company.command.CommandResult;
import com.company.domain.Direction;
import com.company.domain.Position;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.junit.jupiter.api.Test;

import static com.company.domain.Direction.*;
import static org.assertj.core.api.Assertions.assertThat;

class RightCommandTest {

    @Test
    void executeShouldNotChangeDirectionWhenRobotIsNotPlacedOnTable() {
        Table table = new Table(5, 5);
        ToyRobot toyRobot = new ToyRobot(new Position(0, 0), NORTH);

        RightCommand rightCommand = new RightCommand();
        assertThat(rightCommand.execute(table, toyRobot)).isEqualTo(CommandResult.failed("Toy robot has not been placed on table yet.", rightCommand));

        assertThat(toyRobot.getDirection()).isEqualTo(NORTH);
    }

    @Test
    void executeShouldSetTheRobotDirectionToEastWhenCurrentDirectionIsNorth() {
        testDirectionChangeForRightCommand(NORTH, EAST);
    }

    @Test
    void executeShouldSetTheRobotDirectionToSouthWhenCurrentDirectionIsEast() {
        testDirectionChangeForRightCommand(EAST, SOUTH);
    }

    @Test
    void executeShouldSetTheRobotDirectionToWestWhenCurrentDirectionIsSouth() {
        testDirectionChangeForRightCommand(SOUTH, WEST);
    }

    @Test
    void executeShouldSetTheRobotDirectionToNorthWhenCurrentDirectionIsWest() {
        testDirectionChangeForRightCommand(WEST, NORTH);
    }

    private void testDirectionChangeForRightCommand(Direction originalDirection, Direction directionAfterCommand) {
        Table table = new Table(5, 5);
        ToyRobot toyRobot = new ToyRobot(new Position(0, 0), originalDirection);
        table.setToyRobot(toyRobot);

        RightCommand rightCommand = new RightCommand();
        assertThat(rightCommand.execute(table, toyRobot)).isEqualTo(CommandResult.success("The new direction is " + directionAfterCommand, rightCommand));

        assertThat(toyRobot.getDirection()).isEqualTo(directionAfterCommand);
    }
}