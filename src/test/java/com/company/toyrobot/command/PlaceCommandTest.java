package com.company.toyrobot.command;

import com.company.command.CommandResult;
import com.company.domain.Direction;
import com.company.domain.Position;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.junit.jupiter.api.Test;

import static com.company.command.CommandResult.CompletionStatus.SUCCESS;
import static com.company.domain.Direction.*;
import static org.assertj.core.api.Assertions.assertThat;

class PlaceCommandTest {

    @Test
    void executeShouldReturnFailedCommandWhenPositionIsNotOnTable() {
        Table table = new Table(5, 5);
        ToyRobot toyRobot = new ToyRobot(new Position(0, 0), NORTH);

        PlaceCommand placeCommand = new PlaceCommand(new Position(500, 500), NORTH);

        assertThat(placeCommand.execute(table, toyRobot)).isEqualTo(CommandResult.failed("Position is off the table.", placeCommand));
    }

    @Test
    void executeShouldPlaceTheRobotOnTheTableInTheProvidedPositionAndDirectionWhenRobotHasNotBeenPlaced() {
        Table table = new Table(5, 5);
        ToyRobot toyRobot = new ToyRobot(new Position(0, 0), NORTH);

        Position position = new Position(3, 2);
        Direction direction = EAST;
        PlaceCommand placeCommand = new PlaceCommand(position, direction);

        assertThat(placeCommand.execute(table, toyRobot).getCompletionStatus()).isEqualTo(SUCCESS);

        assertThat(table.getToyRobot()).hasValue(toyRobot);
        assertThat(toyRobot.getPosition()).isEqualTo(position);
        assertThat(toyRobot.getDirection()).isEqualTo(direction);
    }

    @Test
    void executeShouldPlaceTheRobotOnTheTableInTheProvidedPositionAndDirectionWhenRobotHasAlreadyBeenPlaced() {
        Table table = new Table(5, 5);
        ToyRobot toyRobot = new ToyRobot(new Position(1, 4), SOUTH);
        table.setToyRobot(toyRobot);

        Position position = new Position(3, 2);
        Direction direction = EAST;
        PlaceCommand placeCommand = new PlaceCommand(position, direction);

        assertThat(placeCommand.execute(table, toyRobot).getCompletionStatus()).isEqualTo(SUCCESS);

        assertThat(table.getToyRobot()).hasValue(toyRobot);
        assertThat(toyRobot.getPosition()).isEqualTo(position);
        assertThat(toyRobot.getDirection()).isEqualTo(direction);
    }
}