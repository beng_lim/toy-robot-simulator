package com.company.toyrobot.command.parser;

import com.company.command.reader.parser.CommandInput;
import com.company.toyrobot.command.MoveCommand;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

class MoveCommandParserTest {

    private MoveCommandParser moveCommandParser = new MoveCommandParser();

    @Test
    void parseShouldReturnAMoveCommandWithOneMovementUnits() {
        assertThat(moveCommandParser.parse(new CommandInput("MOVE", Collections.emptyList()))).isEqualTo(new MoveCommand(1));
    }
}