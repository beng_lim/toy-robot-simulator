package com.company.toyrobot.command.parser;

import com.company.command.Command;
import com.company.command.reader.parser.CommandInput;
import com.company.command.reader.parser.CommandParsingException;
import com.company.domain.Position;
import com.company.toyrobot.command.PlaceCommand;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static com.company.domain.Direction.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PlaceCommandParserTest {

    private PlaceCommandParser placeCommandParser = new PlaceCommandParser();

    @Test
    void getCommandNameShouldReturnPLACE() {
        assertThat(placeCommandParser.getCommandName()).isEqualTo("PLACE");
    }

    @Test
    void parseShouldThrowCommandParsingExceptionWhenNumberOfArgumentsIsLessThan3() {
        assertThrows(CommandParsingException.class, () -> placeCommandParser.parse(new CommandInput("PLACE", Arrays.asList("1", "2"))));
    }

    @Test
    void parseShouldThrowCommandParsingExceptionWhenPositionXIsInvalid() {
        CommandParsingException exception = assertThrows(CommandParsingException.class, () -> placeCommandParser.parse(new CommandInput("PLACE", Arrays.asList("abc", "2", "EAST"))));

        assertThat(exception).hasMessageStartingWith("Invalid position x:");
    }

    @Test
    void parseShouldThrowCommandParsingExceptionWhenPositionYIsInvalid() {
        CommandParsingException exception = assertThrows(CommandParsingException.class, () -> placeCommandParser.parse(new CommandInput("PLACE", Arrays.asList("1", "abc", "EAST"))));

        assertThat(exception).hasMessageStartingWith("Invalid position y:");
    }

    @Test
    void parseShouldThrowCommandParsingExceptionWhenDirectionIsInvalid() {
        CommandParsingException exception = assertThrows(CommandParsingException.class, () -> placeCommandParser.parse(new CommandInput("PLACE", Arrays.asList("1", "2", "abc"))));

        assertThat(exception).hasMessageStartingWith("Invalid direction:");
    }

    @Test
    void parseShouldReturnExpectedPlaceCommandWhenArgumentsAreValid() {
        Command command = placeCommandParser.parse(new CommandInput("PLACE", Arrays.asList("111", "500", "EAST")));

        assertThat(command).isEqualTo(new PlaceCommand(new Position(111, 500), EAST));
    }

    @Test
    void parseShouldReturnExpectedPlaceCommandWhenPositionArgumentsAreNegative() {
        Command command = placeCommandParser.parse(new CommandInput("PLACE", Arrays.asList("-1", "-1", "WEST")));

        assertThat(command).isEqualTo(new PlaceCommand(new Position(-1, -1), WEST));
    }

    @Test
    void parseShouldReturnExpectedPlaceCommandWhenPositionArgumentsAreZero() {
        Command command = placeCommandParser.parse(new CommandInput("PLACE", Arrays.asList("0", "0", "NORTH")));

        assertThat(command).isEqualTo(new PlaceCommand(new Position(0, 0), NORTH));
    }
}