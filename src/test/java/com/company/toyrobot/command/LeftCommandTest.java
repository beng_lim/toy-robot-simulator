package com.company.toyrobot.command;

import com.company.command.CommandResult;
import com.company.domain.Direction;
import com.company.domain.Position;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.junit.jupiter.api.Test;

import static com.company.domain.Direction.*;
import static org.assertj.core.api.Assertions.assertThat;

class LeftCommandTest {

    @Test
    void executeShouldNotChangeDirectionWhenRobotIsNotPlacedOnTable() {
        Table table = new Table(5, 5);
        ToyRobot toyRobot = new ToyRobot(new Position(0, 0), NORTH);

        LeftCommand leftCommand = new LeftCommand();
        assertThat(leftCommand.execute(table, toyRobot)).isEqualTo(CommandResult.failed("Toy robot has not been placed on table yet.", leftCommand));

        assertThat(toyRobot.getDirection()).isEqualTo(NORTH);
    }

    @Test
    void executeShouldSetTheRobotDirectionToWestWhenCurrentDirectionIsNorth() {
        testDirectionChangeForLeftCommand(NORTH, WEST);
    }

    @Test
    void executeShouldSetTheRobotDirectionToSouthWhenCurrentDirectionIsWest() {
        testDirectionChangeForLeftCommand(WEST, SOUTH);
    }

    @Test
    void executeShouldSetTheRobotDirectionToEastWhenCurrentDirectionIsSouth() {
        testDirectionChangeForLeftCommand(SOUTH, EAST);
    }

    @Test
    void executeShouldSetTheRobotDirectionToNorthWhenCurrentDirectionIsEast() {
        testDirectionChangeForLeftCommand(EAST, NORTH);
    }

    private void testDirectionChangeForLeftCommand(Direction originalDirection, Direction directionAfterCommand) {
        Table table = new Table(5, 5);
        ToyRobot toyRobot = new ToyRobot(new Position(0, 0), originalDirection);
        table.setToyRobot(toyRobot);

        LeftCommand leftCommand = new LeftCommand();
        assertThat(leftCommand.execute(table, toyRobot)).isEqualTo(CommandResult.success("The new direction is " + directionAfterCommand, leftCommand));

        assertThat(toyRobot.getDirection()).isEqualTo(directionAfterCommand);
    }
}