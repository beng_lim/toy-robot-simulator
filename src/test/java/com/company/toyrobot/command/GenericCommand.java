package com.company.toyrobot.command;

import com.company.command.Command;
import com.company.command.CommandResult;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class GenericCommand implements Command {

    private int forEqualsCheck;

    public GenericCommand(int forEqualsCheck) {
        this.forEqualsCheck = forEqualsCheck;
    }

    @Override
    public CommandResult execute(Table table, ToyRobot toyRobot) {
        return null;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        GenericCommand that = (GenericCommand) other;

        return new EqualsBuilder()
                .append(forEqualsCheck, that.forEqualsCheck)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(forEqualsCheck)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("forEqualsCheck", forEqualsCheck)
                .toString();
    }
}