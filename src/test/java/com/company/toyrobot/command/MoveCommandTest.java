package com.company.toyrobot.command;

import com.company.command.CommandResult;
import com.company.domain.Position;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static com.company.domain.Direction.*;
import static org.assertj.core.api.Assertions.assertThat;

class MoveCommandTest {

    private Table table = new Table(5, 5);
    private ToyRobot toyRobot = new ToyRobot(new Position(0, 0), NORTH);
    private MoveCommand moveCommand = new MoveCommand(1);

    @BeforeEach
    void setUp() {
        table.setToyRobot(toyRobot);
    }

    @Test
    void executeShouldNotMoveWhenRobotIsNotPlacedOnTable() {
        Table table = new Table(5, 5);
        Position originalPosition = new Position(0, 0);
        ToyRobot toyRobot = new ToyRobot(originalPosition, NORTH);

        assertThat(moveCommand.execute(table, toyRobot)).isEqualTo(CommandResult.failed("Toy robot has not been placed on table yet.", moveCommand));

        assertThat(toyRobot.getPosition()).isEqualTo(originalPosition);
    }

    @Test
    void executeShouldMoveTheRobotOneUnitNorth() {
        toyRobot.setDirection(NORTH);

        Position newPosition = new Position(0, 1);
        assertThat(moveCommand.execute(table, toyRobot)).isEqualTo(CommandResult.success("The new position is " + newPosition, moveCommand));
        assertThat(toyRobot.getPosition()).isEqualTo(newPosition);
    }

    @Test
    void executeShouldMoveTheRobotTwoUnitNorthWhenMoveUnitsIs2() {
        moveCommand = new MoveCommand(2);
        toyRobot.setDirection(NORTH);

        Position newPosition = new Position(0, 2);
        assertThat(moveCommand.execute(table, toyRobot)).isEqualTo(CommandResult.success("The new position is " + newPosition, moveCommand));
        assertThat(toyRobot.getPosition()).isEqualTo(newPosition);
    }

    @Test
    void executeShouldMoveTheRobotOneUnitEast() {
        toyRobot.setDirection(EAST);

        Position newPosition = new Position(1, 0);
        assertThat(moveCommand.execute(table, toyRobot)).isEqualTo(CommandResult.success("The new position is " + newPosition, moveCommand));
        assertThat(toyRobot.getPosition()).isEqualTo(newPosition);
    }

    @Test
    void executeShouldMoveTheRobotOneUnitSouth() {
        toyRobot.setDirection(SOUTH);
        toyRobot.setPosition(new Position(4, 4));

        Position newPosition = new Position(4, 3);
        assertThat(moveCommand.execute(table, toyRobot)).isEqualTo(CommandResult.success("The new position is " + newPosition, moveCommand));
        assertThat(toyRobot.getPosition()).isEqualTo(newPosition);
    }

    @Test
    void executeShouldMoveTheRobotOneUnitWest() {
        toyRobot.setDirection(WEST);
        toyRobot.setPosition(new Position(4, 4));

        Position newPosition = new Position(3, 4);
        assertThat(moveCommand.execute(table, toyRobot)).isEqualTo(CommandResult.success("The new position is " + newPosition, moveCommand));
        assertThat(toyRobot.getPosition()).isEqualTo(newPosition);
    }

    @Test
    void executeMultipleTimesShouldMoveTheRobotNorthUntilItCanNoLongerMove() {
        toyRobot.setDirection(NORTH);

        moveRobotUntilItCanNoLongerMove(4, moveCount -> new Position(0, moveCount), new Position(0, 5));

        assertThat(toyRobot.getPosition()).isEqualTo(new Position(0, 4));
    }

    @Test
    void executeMultipleTimesShouldMoveTheRobotEastUntilItCanNoLongerMove() {
        toyRobot.setDirection(EAST);

        moveRobotUntilItCanNoLongerMove(4, moveCount -> new Position(moveCount, 0), new Position(5, 0));

        assertThat(toyRobot.getPosition()).isEqualTo(new Position(4, 0));
    }

    @Test
    void executeMultipleTimesShouldMoveTheRobotSouthUntilItCanNoLongerMove() {
        toyRobot.setDirection(SOUTH);
        toyRobot.setPosition(new Position(4, 4));

        moveRobotUntilItCanNoLongerMove(4, moveCount -> new Position(4, 4 - moveCount), new Position(4, -1));

        assertThat(toyRobot.getPosition()).isEqualTo(new Position(4, 0));
    }

    @Test
    void executeMultipleTimesShouldMoveTheRobotWestUntilItCanNoLongerMove() {
        toyRobot.setDirection(WEST);
        toyRobot.setPosition(new Position(4, 4));

        moveRobotUntilItCanNoLongerMove(4, moveCount -> new Position(4 - moveCount, 4), new Position(-1, 4));

        assertThat(toyRobot.getPosition()).isEqualTo(new Position(0, 4));
    }

    @Test
    void executeMultipleTimesAllDirectionsShouldWalkFullCircleTestingEachBoundary() {
        toyRobot.setDirection(NORTH);
        moveRobotUntilItCanNoLongerMove(4, moveCount -> new Position(0, moveCount), new Position(0, 5));
        toyRobot.setDirection(EAST);
        moveRobotUntilItCanNoLongerMove(4, moveCount -> new Position(moveCount, 4), new Position(5, 4));
        toyRobot.setDirection(SOUTH);
        moveRobotUntilItCanNoLongerMove(4, moveCount -> new Position(4, 4 - moveCount), new Position(4, -1));
        toyRobot.setDirection(WEST);
        moveRobotUntilItCanNoLongerMove(4, moveCount -> new Position(4 - moveCount, 0), new Position(-1, 0));

        assertThat(toyRobot.getPosition()).isEqualTo(new Position(0, 0));
    }

    private void moveRobotUntilItCanNoLongerMove(
            int expectedNumberOfMoves,
            Function<Integer, Position> newPositionSupplier,
            Position failedPosition
    ) {
        for (int i = 0; i < expectedNumberOfMoves; i++) {
            Position newPosition = newPositionSupplier.apply(i + 1);
            assertThat(moveCommand.execute(table, toyRobot)).isEqualTo(CommandResult.success("The new position is " + newPosition, moveCommand));
        }
        assertThat(moveCommand.execute(table, toyRobot)).isEqualTo(CommandResult.failed("New position " + failedPosition + " will result in the toy robot falling off the table.", moveCommand));
    }
}