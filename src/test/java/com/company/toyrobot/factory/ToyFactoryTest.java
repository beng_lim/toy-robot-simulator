package com.company.toyrobot.factory;

import com.company.domain.Direction;
import com.company.domain.Position;
import com.company.toyrobot.ToyRobot;
import org.junit.jupiter.api.Test;

import static com.company.domain.Direction.NORTH;
import static com.company.domain.Direction.SOUTH;
import static org.assertj.core.api.Assertions.assertThat;

class ToyFactoryTest {

    private ToyRobotFactory toyRobotFactory = new ToyRobotFactory();

    @Test
    void createToyRobotWithNoArgsShouldReturnToyRobotWithDefaultPositionAndNorthDirection() {
        ToyRobot toyRobot = toyRobotFactory.createToyRobot();
        assertThat(toyRobot.getPosition()).isEqualTo(new Position(0, 0));
        assertThat(toyRobot.getDirection()).isEqualTo(NORTH);
    }

    @Test
    void createToyRobotWithArgsShouldReturnToyRobotWithProvidedPositionAndDirection() {
        Position position = new Position(6, 2);
        Direction direction = SOUTH;

        ToyRobot toyRobot = toyRobotFactory.createToyRobot(position, direction);

        assertThat(toyRobot.getPosition()).isEqualTo(position);
        assertThat(toyRobot.getDirection()).isEqualTo(direction);
    }
}