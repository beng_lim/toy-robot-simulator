package com.company;

import com.company.command.reader.parser.CommandParsingException;
import com.company.domain.ApplicationExecutionContext;
import com.company.domain.Direction;
import com.company.domain.Position;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.assertj.core.util.Files;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.company.domain.Direction.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.parallel.ExecutionMode.SAME_THREAD;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;

@ExtendWith(SpringExtension.class)
@Configuration
class ToyRobotApplicationTest {

    @Bean
    @Qualifier("outputWriter")
    public Writer outputWriter() {
        return new StringWriter();
    }

    @Bean
    @Qualifier("errorWriter")
    public Writer errorWriter() {
        return new StringWriter();
    }

    @Nested
    @SpringBootTest(classes = {ToyRobotApplication.class, ToyRobotApplicationTest.class})
    @ActiveProfiles({"file", "unit-test", "test-output"})
    @Execution(SAME_THREAD) // Avoid sharing the output/error writers.
    @DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
            // Clear the output/error writers after each test.
    class FileInput {

        private final Path INPUT_FILE_PATH = Paths.get("src/test/resources/input_files");
        private final Path OUTPUT_FILE_PATH = Paths.get("src/test/resources/input_files/output/stdout");
        private final Path ERROR_FILE_PATH = Paths.get("src/test/resources/input_files/output/stderr");

        @Autowired
        private ToyRobotApplication toyRobotApplication;
        @Autowired
        @Qualifier("outputWriter")
        private Writer outputWriter;
        @Autowired
        @Qualifier("errorWriter")
        private Writer errorWriter;

        @Test
        void endToEndTestForSampleAFile() {
            readInputFileAndAssert("sample_a.txt", 5, 5, new Position(0, 1), NORTH);
        }

        @Test
        void endToEndTestForSampleBFile() {
            readInputFileAndAssert("sample_b.txt", 5, 5, new Position(0, 0), WEST);
        }

        @Test
        void endToEndTestForSampleCFile() {
            readInputFileAndAssert("sample_c.txt", 5, 5, new Position(3, 3), NORTH);
        }

        @Test
        void endToEndTestForSampleDFile() {
            readInputFileAndAssert("sample_d.txt", 5, 5, new Position(0, 4), SOUTH);
        }

        @Test
        void endToEndTestForSampleEFile() {
            readInputFileAndAssert("sample_e.txt", 5, 5, new Position(2, 3), SOUTH);
        }

        @Test
        void startShouldNotSetRobotOnTableIfNoLinesToProcess() {
            ApplicationExecutionContext executionContext =
                    toyRobotApplication.start(new String[]{INPUT_FILE_PATH.resolve("empty.txt").toString()});


            Table table = executionContext.getTable();
            assertTableSize(table, 5, 5);
            assertThat(table.getToyRobot()).isEmpty();
        }

        @Test
        void startShouldNotSetRobotOnTableIfOnlyEmptyLines() {
            ApplicationExecutionContext executionContext =
                    toyRobotApplication.start(new String[]{INPUT_FILE_PATH.resolve("empty_lines.txt").toString()});


            Table table = executionContext.getTable();
            assertTableSize(table, 5, 5);
            assertThat(table.getToyRobot()).isEmpty();
        }

        @Test
        void startShouldThrowCommandParsingExceptionWhenInvalidCommandArgumentsProvided() {
            assertThrows(CommandParsingException.class,
                    () -> toyRobotApplication.start(new String[]{INPUT_FILE_PATH.resolve("bad_arguments.txt").toString()}));
        }

        @Test
        void startShouldThrowUnsupportedOperationExceptionWhenEncounteringBadCommandName() {
            assertThrows(UnsupportedOperationException.class,
                    () -> toyRobotApplication.start(new String[]{INPUT_FILE_PATH.resolve("bad_commands.txt").toString()}));
        }

        /**
         * Passes the input file name into the application and asserts the following:
         * - Table width and height
         * - Toy robot position and direction
         * - Output against the output/stdout/ file
         * - Errors against the output/stderr/ file
         *
         * @param inputFileName
         * @param tableWidth
         * @param tableHeight
         * @param toyRobotEndingPosition
         * @param toyRobotEndingDirection
         */
        private void readInputFileAndAssert(String inputFileName, int tableWidth, int tableHeight,
                                            Position toyRobotEndingPosition, Direction toyRobotEndingDirection) {
            ApplicationExecutionContext executionContext =
                    toyRobotApplication.start(new String[]{INPUT_FILE_PATH.resolve(inputFileName).toString()});

            Table table = executionContext.getTable();
            assertTableSize(table, tableWidth, tableHeight);
            ToyRobot toyRobot = table.getToyRobot().orElseThrow(() -> new RuntimeException("Expected robot not to be empty."));
            assertToyRobot(toyRobot, toyRobotEndingPosition, toyRobotEndingDirection);

            assertThat(outputWriter).hasToString(Files.contentOf(OUTPUT_FILE_PATH.resolve(inputFileName).toFile(), UTF_8));
            assertThat(errorWriter).hasToString(Files.contentOf(ERROR_FILE_PATH.resolve(inputFileName).toFile(), UTF_8));
        }

        private void assertToyRobot(ToyRobot toyRobot, Position position, Direction direction) {
            assertThat(toyRobot.getPosition()).isEqualTo(position);
            assertThat(toyRobot.getDirection()).isEqualTo(direction);
        }

        private void assertTableSize(Table table, int expectedWidth, int expectedHeight) {
            assertThat(table.getWidth()).isEqualTo(expectedWidth);
            assertThat(table.getHeight()).isEqualTo(expectedHeight);
        }
    }

    @Nested
    @SpringBootTest(classes = {ToyRobotApplication.class, ToyRobotApplicationTest.class})
    @ActiveProfiles({"std-in", "unit-test"})
    class StandardInput {

        @Autowired
        private ToyRobotApplication toyRobotApplication;

        @Test
        void shouldThrowUnsupportedOperationException() {
            assertThrows(UnsupportedOperationException.class, () -> toyRobotApplication.start(new String[]{}));
        }
    }
}