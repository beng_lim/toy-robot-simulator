package com.company.domain;

import org.junit.jupiter.api.Test;

import static com.company.domain.Direction.*;
import static org.assertj.core.api.Assertions.assertThat;

class DirectionTest {

    @Test
    void rotateClockwiseShouldReturnEastWhenCurrentDirectionIsNorth() {
        assertThat(NORTH.rotateClockwise()).isEqualTo(EAST);
    }

    @Test
    void rotateClockwiseShouldReturnSouthWhenCurrentDirectionIsEast() {
        assertThat(EAST.rotateClockwise()).isEqualTo(SOUTH);
    }

    @Test
    void rotateClockwiseShouldReturnWestWhenCurrentDirectionIsSouth() {
        assertThat(SOUTH.rotateClockwise()).isEqualTo(WEST);
    }

    @Test
    void rotateClockwiseShouldReturnNorthWhenCurrentDirectionIsWest() {
        assertThat(WEST.rotateClockwise()).isEqualTo(NORTH);
    }

    @Test
    void rotateAntiClockwiseShouldReturnWestWhenCurrentDirectionIsNorth() {
        assertThat(NORTH.rotateAntiClockwise()).isEqualTo(WEST);
    }

    @Test
    void rotateAntiClockwiseShouldReturnSouthWhenCurrentDirectionIsWest() {
        assertThat(WEST.rotateAntiClockwise()).isEqualTo(SOUTH);
    }

    @Test
    void rotateAntiClockwiseShouldReturnEastWhenCurrentDirectionIsSouth() {
        assertThat(SOUTH.rotateAntiClockwise()).isEqualTo(EAST);
    }

    @Test
    void rotateAntiClockwiseShouldReturnNorthWhenCurrentDirectionIsEast() {
        assertThat(EAST.rotateAntiClockwise()).isEqualTo(NORTH);
    }
}