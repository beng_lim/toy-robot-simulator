package com.company.table.configuration;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles({"file", "unit-test"})
class DefaultTableConfigurationTest {

    @Autowired
    private DefaultTableConfiguration defaultTableConfiguration;

    @Test
    void shouldHaveTheExpectedWidth() {
        assertThat(defaultTableConfiguration.getWidth()).isEqualTo(5);
    }

    @Test
    void shouldHaveTheExpectedHeight() {
        assertThat(defaultTableConfiguration.getHeight()).isEqualTo(5);
    }
}