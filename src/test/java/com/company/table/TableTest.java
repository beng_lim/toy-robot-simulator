package com.company.table;

import com.company.domain.Direction;
import com.company.domain.Position;
import com.company.toyrobot.ToyRobot;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TableTest {

    private Table table = new Table(5, 5);

    @Test
    void isPositionOnTableShouldReturnTrueWhenAtMaxBoundForXAndMaxBoundForY() {
        assertThat(table.isPositionOnTable(new Position(4, 4))).isTrue();
    }

    @Test
    void isPositionOnTableShouldReturnTrueWhenAtMinBoundForXAndMinBoundForY() {
        assertThat(table.isPositionOnTable(new Position(0, 0))).isTrue();
    }

    @Test
    void isPositionOnTableShouldReturnTrueWhenAtMaxBoundForX() {
        assertThat(table.isPositionOnTable(new Position(4, 2))).isTrue();
    }

    @Test
    void isPositionOnTableShouldReturnTrueWhenAtMinBoundForX() {
        assertThat(table.isPositionOnTable(new Position(0, 2))).isTrue();
    }

    @Test
    void isPositionOnTableShouldReturnTrueWhenAtMaxBoundForY() {
        assertThat(table.isPositionOnTable(new Position(2, 4))).isTrue();
    }

    @Test
    void isPositionOnTableShouldReturnTrueWhenAtMinBoundForY() {
        assertThat(table.isPositionOnTable(new Position(2, 0))).isTrue();
    }

    @Test
    void isPositionOnTableShouldReturnFalseWhenGreaterThanMaxBoundForX() {
        assertThat(table.isPositionOnTable(new Position(6, 2))).isFalse();
    }

    @Test
    void isPositionOnTableShouldReturnFalseWhenLowerThanMinBoundForX() {
        assertThat(table.isPositionOnTable(new Position(-1, 2))).isFalse();
    }

    @Test
    void isPositionOnTableShouldReturnFalseWhenGreaterThanMaxBoundForY() {
        assertThat(table.isPositionOnTable(new Position(2, 6))).isFalse();
    }

    @Test
    void isPositionOnTableShouldReturnFalseWhenLowerThanMinBoundForY() {
        assertThat(table.isPositionOnTable(new Position(2, -1))).isFalse();
    }

    @Test
    void isPositionOnTableShouldReturnFalseWhenLowerThanMinBoundForXAndLowerThanMinBoundForY() {
        assertThat(table.isPositionOnTable(new Position(-1, -1))).isFalse();
    }

    @Test
    void isPositionOnTableShouldReturnFalseWhenEqualToWidthForX() {
        assertThat(table.isPositionOnTable(new Position(5, 2))).isFalse();
    }

    @Test
    void isPositionOnTableShouldReturnFalseWhenEqualToHeightForY() {
        assertThat(table.isPositionOnTable(new Position(2, 5))).isFalse();
    }

    @Test
    void setToyRobotShouldThrowPositionOffTableExceptionWhenToyRobotPositionIsOutOfBounds() {
        assertThrows(PositionOffTableException.class, () -> table.setToyRobot(new ToyRobot(new Position(100, 100), Direction.NORTH)));
    }
}