package com.company.table.factory;

import com.company.table.Table;
import com.company.table.configuration.DefaultTableConfiguration;
import com.company.table.configuration.TableConfiguration;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TableFactoryTest {

    private int width = 51;
    private int height = 20;

    private TableConfiguration tableConfiguration = new DefaultTableConfiguration(width, height);
    private TableFactory tableFactory = new TableFactory(tableConfiguration);

    @Test
    void createDefaultTableShouldHaveExpectedWidth() {
        Table table = tableFactory.createDefaultTable();
        assertThat(table.getWidth()).isEqualTo(width);
    }

    @Test
    void createDefaultTableShouldHaveExpectedHeight() {
        Table table = tableFactory.createDefaultTable();
        assertThat(table.getHeight()).isEqualTo(height);
    }
}