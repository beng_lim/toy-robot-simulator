package com.company.table.factory;

import com.company.table.Table;
import com.company.table.configuration.TableConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TableFactory {

    private static final Logger log = LoggerFactory.getLogger(TableFactory.class);

    private final TableConfiguration tableConfiguration;

    public TableFactory(TableConfiguration tableConfiguration) {
        this.tableConfiguration = tableConfiguration;
    }

    public Table createDefaultTable() {
        log.info("Creating table with default configuration. Width - {}, Height - {}",
                tableConfiguration.getWidth(), tableConfiguration.getHeight());
        return new Table(tableConfiguration.getWidth(), tableConfiguration.getHeight());
    }
}