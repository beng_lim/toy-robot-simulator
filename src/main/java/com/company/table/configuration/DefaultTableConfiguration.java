package com.company.table.configuration;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

@Configuration
@ConfigurationProperties(prefix = "toy-robot-simulator.table.default")
public class DefaultTableConfiguration implements TableConfiguration {

    private static final Logger log = LoggerFactory.getLogger(DefaultTableConfiguration.class);

    private int width;
    private int height;

    public DefaultTableConfiguration() { // For Spring's magic
    }

    public DefaultTableConfiguration(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @PostConstruct
    private void printConfiguration() {
        log.info("Running with table configuration {}", this);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("width", width)
                .append("height", height)
                .toString();
    }
}
