package com.company.table.configuration;

public interface TableConfiguration {

    int getWidth();

    int getHeight();
}
