package com.company.table;

import com.company.domain.Position;
import com.company.table.factory.TableFactory;
import com.company.toyrobot.ToyRobot;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class Table {

    private static final Logger log = LoggerFactory.getLogger(TableFactory.class);

    private int width;
    private int height;

    private int minPositionX;
    private int maxPositionX;
    private int minPositionY;
    private int maxPositionY;

    private ToyRobot toyRobot;

    public Table(int width, int height) {
        this.width = width;
        this.height = height;

        initialiseBoundaries(width, height);

        log.info("Created table with properties: {}", this);
    }

    private void initialiseBoundaries(int width, int height) {
        minPositionX = 0;
        maxPositionX = width - 1;
        minPositionY = 0;
        maxPositionY = height - 1;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Optional<ToyRobot> getToyRobot() {
        return Optional.ofNullable(toyRobot);
    }

    public void setToyRobot(ToyRobot toyRobot) {
        validateToyRobotPosition(toyRobot);
        this.toyRobot = toyRobot;
    }

    private void validateToyRobotPosition(ToyRobot toyRobot) {
        Position position = toyRobot.getPosition();
        if (!isPositionOnTable(position)) {
            throw new PositionOffTableException("Position is not valid for this table: " + position);
        }
    }

    public boolean isPositionOnTable(Position position) {
        return position.getX() >= minPositionX
                && position.getX() <= maxPositionX
                && position.getY() >= minPositionY
                && position.getY() <= maxPositionY;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("width", width)
                .append("height", height)
                .append("minPositionX", minPositionX)
                .append("maxPositionX", maxPositionX)
                .append("minPositionY", minPositionY)
                .append("maxPositionY", maxPositionY)
                .append("toyRobot", toyRobot)
                .toString();
    }
}
