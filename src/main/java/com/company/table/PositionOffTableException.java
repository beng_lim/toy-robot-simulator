package com.company.table;

public class PositionOffTableException extends RuntimeException {

    public PositionOffTableException(String message) {
        super(message);
    }

    public PositionOffTableException(String message, Throwable cause) {
        super(message, cause);
    }
}
