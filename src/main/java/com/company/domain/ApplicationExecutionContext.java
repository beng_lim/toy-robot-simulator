package com.company.domain;

import com.company.command.CommandResult;
import com.company.table.Table;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class ApplicationExecutionContext {

    private Table table;
    private List<CommandResult> commandResults;

    public ApplicationExecutionContext(Table table, List<CommandResult> commandResults) {
        this.table = table;
        this.commandResults = commandResults;
    }

    public Table getTable() {
        return table;
    }

    public List<CommandResult> getCommandResults() {
        return commandResults;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("table", table)
                .append("commandResults", commandResults)
                .toString();
    }


}
