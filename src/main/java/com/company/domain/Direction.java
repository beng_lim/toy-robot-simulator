package com.company.domain;

public enum Direction {

    NORTH(0, 1), // NOTE: The ordering of these enum values matter.
    EAST(1, 0),
    SOUTH(0, -1),
    WEST(-1, 0);

    private int positionModifierX;
    private int positionModifierY;

    Direction(int positionModifierX, int positionModifierY) {
        this.positionModifierX = positionModifierX;
        this.positionModifierY = positionModifierY;
    }

    private static final Direction[] VALUES = Direction.values();

    public Direction rotateClockwise() {
        return VALUES[(ordinal() + 1) % VALUES.length];
    }

    public Direction rotateAntiClockwise() {
        int previousIndex = ordinal() - 1;
        if (previousIndex < 0) { // wrap around
            previousIndex = VALUES.length - 1;
        }
        return VALUES[previousIndex];
    }

    public int getPositionModifierX() {
        return positionModifierX;
    }

    public int getPositionModifierY() {
        return positionModifierY;
    }
}
