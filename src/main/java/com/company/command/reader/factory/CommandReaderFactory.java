package com.company.command.reader.factory;

import com.company.command.reader.CommandReader;

@FunctionalInterface
public interface CommandReaderFactory {

    CommandReader fromArguments(String[] args);
}