package com.company.command.reader.factory;

import com.company.command.Command;
import com.company.command.reader.parser.CommandInput;
import com.company.command.reader.parser.CommandInputParser;
import com.company.command.reader.parser.CommandParser;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CommandFactory {

    private static final Logger log = LoggerFactory.getLogger(CommandFactory.class);

    private final CommandInputParser commandInputParser;
    private final Map<String, CommandParser> commandParsers;

    public CommandFactory(CommandInputParser commandInputParser, @Qualifier("commandParsers") Map<String, CommandParser> commandParsers) {
        this.commandInputParser = commandInputParser;
        this.commandParsers = commandParsers;
    }

    public Command createCommand(String rawCommand) {
        Validate.notEmpty(rawCommand, "Command input must be provided");

        CommandInput commandInput = commandInputParser.parseRawCommand(rawCommand);
        CommandParser commandParser = getCommandParser(commandInput);
        return commandParser.parse(commandInput);
    }

    private CommandParser getCommandParser(CommandInput commandInput) {
        CommandParser commandParser = commandParsers.get(commandInput.getCommandName());
        if (commandParser == null) {
            throw new UnsupportedOperationException("Could not find command parser for input: "
                    + commandInput.getCommandName());
        }
        log.info("Parsing command with {} and input {}", commandParser.getClass().getSimpleName(), commandInput);
        return commandParser;
    }
}