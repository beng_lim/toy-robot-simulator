package com.company.command.reader.configuration;

import com.company.command.reader.parser.CommandParser;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

@Configuration
public class CommandConfiguration {

    @Bean
    @Qualifier("commandParsers")
    public Map<String, CommandParser> commandParsers(List<CommandParser> commandParsers) {
        return commandParsers.stream()
                .collect(toMap(this::getCommandName, Function.identity()));
    }

    private String getCommandName(CommandParser commandParser) {
        return commandParser.getCommandName().toUpperCase();
    }
}