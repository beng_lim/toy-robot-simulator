package com.company.command.reader.configuration;

import com.company.command.reader.factory.CommandReaderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.util.Arrays;

@Configuration
@Lazy
public class CommandReaderConfiguration {

    public static final String COMMAND_READER_FILE_SOURCE = "file";
    public static final String COMMAND_READER_STD_IN_SOURCE = "std-in";
    public static final String[] COMMAND_READER_SOURCES = new String[]{
            COMMAND_READER_FILE_SOURCE, COMMAND_READER_STD_IN_SOURCE
    };

    @Bean
    public CommandReaderFactory defaultCommandReaderFactory() {
        throw new RuntimeException("One of the following must be an active profile: "
                + Arrays.toString(COMMAND_READER_SOURCES));
    }
}
