package com.company.command.reader.stdin;

import com.company.command.reader.CommandReader;
import com.company.command.reader.factory.CommandReaderFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import static com.company.command.reader.configuration.CommandReaderConfiguration.COMMAND_READER_STD_IN_SOURCE;

@Component
@Profile(COMMAND_READER_STD_IN_SOURCE)
@Primary
public class StandardInputCommandReaderFactory implements CommandReaderFactory {

    @Override
    public CommandReader fromArguments(String[] args) {
        return new StandardInputCommandReader();
    }
}