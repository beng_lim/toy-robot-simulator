package com.company.command.reader.stdin;

import com.company.command.Command;
import com.company.command.reader.CommandReader;

import java.util.stream.Stream;

public class StandardInputCommandReader implements CommandReader {

    public StandardInputCommandReader() {
    }

    @Override
    public Stream<Command> read() {
        throw new UnsupportedOperationException("Standard input command reader is not supported yet.");
    }

    @Override
    public void close() {
        throw new UnsupportedOperationException("Standard input command reader is not supported yet.");
    }
}