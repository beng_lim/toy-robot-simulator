package com.company.command.reader.parser;

import com.company.command.Command;

public interface CommandParser {

    String getCommandName();

    Command parse(CommandInput commandInput);
}