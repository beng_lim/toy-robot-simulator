package com.company.command.reader.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class CommandInputParser {

    private static final Logger log = LoggerFactory.getLogger(CommandInputParser.class);

    private static final String COMMAND_SEPARATOR = " ";
    private static final String ARGUMENTS_SEPARATOR = ",";

    public CommandInput parseRawCommand(String rawCommand) {
        if (rawCommand.isEmpty()) {
            throw new CommandParsingException("The raw command string must be provided");
        }

        log.info("Parsing command {}", rawCommand);
        String[] rawCommandSplits = rawCommand.split(COMMAND_SEPARATOR);

        String commandName = getCommandName(rawCommandSplits);
        List<String> commandArguments = getCommandArguments(rawCommandSplits);

        CommandInput commandInput = new CommandInput(commandName, commandArguments);
        log.info("Parsed command input {}", commandInput);
        return commandInput;
    }

    private String getCommandName(String[] rawCommandSplits) {
        return rawCommandSplits[0].toUpperCase();
    }

    private List<String> getCommandArguments(String[] rawCommandSplits) {
        List<String> commandArguments = Collections.emptyList();
        if (rawCommandSplits.length > 1) {
            commandArguments = Arrays.asList(rawCommandSplits[1].split(ARGUMENTS_SEPARATOR));
        }
        return commandArguments;
    }
}
