package com.company.command.reader.parser;

public class CommandParsingException extends RuntimeException {

    public CommandParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandParsingException(String message) {
        super(message);
    }
}
