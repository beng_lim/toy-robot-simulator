package com.company.command.reader;

import com.company.command.Command;

import java.util.stream.Stream;

public interface CommandReader extends AutoCloseable {

    Stream<Command> read();
}