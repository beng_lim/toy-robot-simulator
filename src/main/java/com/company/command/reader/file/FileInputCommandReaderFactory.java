package com.company.command.reader.file;

import com.company.command.reader.CommandReader;
import com.company.command.reader.factory.CommandFactory;
import com.company.command.reader.factory.CommandReaderFactory;
import org.apache.commons.lang3.Validate;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.company.command.reader.configuration.CommandReaderConfiguration.COMMAND_READER_FILE_SOURCE;

@Component
@Profile(COMMAND_READER_FILE_SOURCE)
@Primary
public class FileInputCommandReaderFactory implements CommandReaderFactory {

    private static final int MINIMUM_EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final int INPUT_FILE_INDEX = 0;

    private final CommandFactory commandFactory;

    public FileInputCommandReaderFactory(CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public CommandReader fromArguments(String[] args) {
        Validate.isTrue(args.length >= MINIMUM_EXPECTED_NUMBER_OF_ARGUMENTS,
                "The number of arguments must be at least " + MINIMUM_EXPECTED_NUMBER_OF_ARGUMENTS);

        String inputFile = args[INPUT_FILE_INDEX];

        try {
            return new FileInputCommandReader(commandFactory, inputFile);
        } catch (IOException e) {
            throw new RuntimeException("Failed to instantiate FileInputCommandReader", e);
        }
    }
}
