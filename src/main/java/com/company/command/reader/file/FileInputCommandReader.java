package com.company.command.reader.file;

import com.company.command.Command;
import com.company.command.reader.CommandReader;
import com.company.command.reader.factory.CommandFactory;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileInputCommandReader implements CommandReader {

    private static final Logger log = LoggerFactory.getLogger(FileInputCommandReader.class);

    private final CommandFactory commandFactory;
    private final Stream<String> fileStream;
    private final Path filePath;

    public FileInputCommandReader(CommandFactory commandFactory, String inputFile) throws IOException {
        filePath = Paths.get(inputFile);

        if (!Files.isRegularFile(filePath)) {
            throw new FileNotFoundException(inputFile + " cannot be found.");
        }

        log.info("Input file for command reader is {}", filePath);
        this.commandFactory = commandFactory;
        fileStream = Files.lines(filePath);
    }

    @Override
    public Stream<Command> read() {
        return fileStream
                .filter(Strings::isNotEmpty)
                .map(commandFactory::createCommand);
    }

    public Path getFilePath() {
        return filePath;
    }

    @Override
    public void close() {
        fileStream.close();
    }
}