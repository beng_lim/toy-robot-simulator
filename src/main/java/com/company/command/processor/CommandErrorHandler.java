package com.company.command.processor;

import com.company.command.CommandResult;
import com.company.command.configuration.CommandErrorHandlerConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;

import static com.company.command.CommandResult.CompletionStatus.FAILED;

@Component
public class CommandErrorHandler {

    private static final Logger log = LoggerFactory.getLogger(CommandErrorHandler.class);

    private final CommandErrorHandlerConfiguration commandErrorHandlerConfiguration;
    private final Writer errorWriter;

    public CommandErrorHandler(
            CommandErrorHandlerConfiguration commandErrorHandlerConfiguration,
            @Qualifier("errorWriter") Writer errorWriter
    ) {
        this.commandErrorHandlerConfiguration = commandErrorHandlerConfiguration;
        this.errorWriter = errorWriter;
    }

    public void handleError(CommandResult commandResult) {
        if (commandResult.getCompletionStatus() != FAILED) {
            log.warn("Command error handler was called with a non failed command result: {}", commandResult);
            return;
        }

        tryWriteFailedMessage(commandResult);

        if (commandErrorHandlerConfiguration.isFailFastOnError()) {
            throw new CommandFailedException(commandResult.getCommand(),
                    String.format("Failed to process command '%s' due to: %s", commandResult.getCommand(), commandResult.getMessage()),
                    commandResult.getException());
        }
    }

    private void tryWriteFailedMessage(CommandResult commandResult) {
        try {
            errorWriter.write(String.format("%s: %s", commandResult.getCommand(), commandResult.getMessage()) + System.lineSeparator());
        } catch (IOException ex) {
            log.error("Could not log command result to error writer. Command result: {}", commandResult, ex);
        }
    }
}
