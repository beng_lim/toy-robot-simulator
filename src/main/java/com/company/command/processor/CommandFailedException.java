package com.company.command.processor;

import com.company.command.Command;

public class CommandFailedException extends RuntimeException {

    private Command command;

    public CommandFailedException(Command command, String message) {
        super(message);
        this.command = command;
    }

    public CommandFailedException(Command command, String message, Throwable cause) {
        super(message, cause);
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }
}
