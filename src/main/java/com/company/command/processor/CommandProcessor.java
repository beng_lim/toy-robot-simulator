package com.company.command.processor;

import com.company.command.Command;
import com.company.command.CommandResult;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static com.company.command.CommandResult.CompletionStatus.FAILED;

@Component
public class CommandProcessor {

    private static final Logger log = LoggerFactory.getLogger(CommandProcessor.class);

    private final CommandErrorHandler commandErrorWriter;

    public CommandProcessor(CommandErrorHandler commandErrorWriter) {
        this.commandErrorWriter = commandErrorWriter;
    }

    public CommandResult processCommand(Table table, ToyRobot toyRobot, Command command) {
        if (!validateSameRobotOnTableIfPresent(table, toyRobot)) {
            return CommandResult.failed("The toy robot on the table is not the same one for this command.", command);
        }
        log.info("Executing command: {}", command);
        CommandResult commandResult = command.execute(table, toyRobot);
        handleResult(commandResult);
        return commandResult;
    }

    private boolean validateSameRobotOnTableIfPresent(Table table, ToyRobot toyRobot) {
        return table.getToyRobot()
                .map(toyRobotOnTable -> toyRobotOnTable == toyRobot)
                .orElse(true); // If it's not set.
    }

    private void handleResult(CommandResult commandResult) {
        if (commandResult.getCompletionStatus() == FAILED) {
            commandErrorWriter.handleError(commandResult);
            log.warn("Command failed: {}", commandResult);
        } else {
            log.info("Command result: {}", commandResult);
        }
    }
}
