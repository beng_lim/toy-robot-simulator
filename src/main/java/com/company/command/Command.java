package com.company.command;

import com.company.table.Table;
import com.company.toyrobot.ToyRobot;

@FunctionalInterface
public interface Command {

    CommandResult execute(Table table, ToyRobot toyRobot);
}