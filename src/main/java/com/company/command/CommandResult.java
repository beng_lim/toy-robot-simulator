package com.company.command;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static com.company.command.CommandResult.CompletionStatus.FAILED;
import static com.company.command.CommandResult.CompletionStatus.SUCCESS;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class CommandResult {

    private final CompletionStatus completionStatus;
    private final String message;
    private final Command command;
    private final Exception exception;

    private CommandResult(CompletionStatus completionStatus, String message, Command command, Exception exception) {
        this.completionStatus = completionStatus;
        this.message = message;
        this.command = command;
        this.exception = exception;
    }

    public CompletionStatus getCompletionStatus() {
        return completionStatus;
    }

    public String getMessage() {
        return message;
    }

    public static CommandResult success(Command originalCommand) {
        return success(null, originalCommand);
    }

    public static CommandResult success(String message, Command originalCommand) {
        return new CommandResult(SUCCESS, message, originalCommand, null);
    }

    public static CommandResult failed(String message, Command originalCommand) {
        return failed(message, originalCommand, null);
    }

    public static CommandResult failed(String message, Command originalCommand, Exception exception) {
        return new CommandResult(FAILED, message, originalCommand, exception);
    }

    public Command getCommand() {
        return command;
    }

    public Exception getException() {
        return exception;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        CommandResult that = (CommandResult) other;

        return new EqualsBuilder()
                .append(completionStatus, that.completionStatus)
                .append(message, that.message)
                .append(command, that.command)
                .append(exception, that.exception)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(completionStatus)
                .append(message)
                .append(command)
                .append(exception)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("completionStatus", completionStatus)
                .append("message", message)
                .append("command", command)
                .append("exception", exception)
                .toString();
    }

    public enum CompletionStatus {

        SUCCESS,
        FAILED
    }
}