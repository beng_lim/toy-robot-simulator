package com.company.command.configuration;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

@Configuration
@ConfigurationProperties(prefix = "toy-robot-simulator.error-handler")
public class CommandErrorHandlerConfiguration {

    private static final Logger log = LoggerFactory.getLogger(CommandErrorHandlerConfiguration.class);

    private boolean failFastOnError;

    public CommandErrorHandlerConfiguration() {
    }

    public CommandErrorHandlerConfiguration(boolean failFastOnError) {
        this.failFastOnError = failFastOnError;
    }

    @PostConstruct
    private void printConfiguration() {
        log.info("Running with command error handler configuration {}", this);
    }

    public boolean isFailFastOnError() {
        return failFastOnError;
    }

    public void setFailFastOnError(boolean failFastOnError) {
        this.failFastOnError = failFastOnError;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("failFastOnError", failFastOnError)
                .toString();
    }
}
