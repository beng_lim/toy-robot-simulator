package com.company.command.writer.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;

import java.io.PrintWriter;
import java.io.Writer;

@Configuration
@Profile("!test-output")
@Lazy
public class CommandWriterConfiguration {

    @Bean
    @Qualifier("outputWriter")
    public Writer outputWriter() {
        return new PrintWriter(System.out);
    }

    @Bean
    @Qualifier("errorWriter")
    public Writer errorWriter() {
        return new PrintWriter(System.err);
    }
}