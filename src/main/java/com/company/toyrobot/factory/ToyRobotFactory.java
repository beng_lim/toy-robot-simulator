package com.company.toyrobot.factory;

import com.company.domain.Direction;
import com.company.domain.Position;
import com.company.toyrobot.ToyRobot;
import org.springframework.stereotype.Component;

import static com.company.domain.Direction.NORTH;

@Component
public class ToyRobotFactory {

    public ToyRobot createToyRobot() {
        return createToyRobot(new Position(0, 0), NORTH);
    }

    public ToyRobot createToyRobot(Position position, Direction direction) {
        return new ToyRobot(position, direction);
    }
}