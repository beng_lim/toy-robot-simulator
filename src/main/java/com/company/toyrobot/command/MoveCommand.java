package com.company.toyrobot.command;

import com.company.command.Command;
import com.company.command.CommandResult;
import com.company.domain.Direction;
import com.company.domain.Position;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class MoveCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(MoveCommand.class);

    private int unitsToMove;

    public MoveCommand(int unitsToMove) {
        this.unitsToMove = unitsToMove;
    }

    @Override
    public CommandResult execute(Table table, ToyRobot toyRobot) {
        if (!table.getToyRobot().isPresent()) {
            return CommandResult.failed("Toy robot has not been placed on table yet.", this);
        }

        Position newPosition = calculateNewPosition(toyRobot.getPosition(), toyRobot.getDirection());
        log.info("Calculated new position is {}", newPosition);
        return setNewPosition(table, toyRobot, newPosition);
    }

    private CommandResult setNewPosition(Table table, ToyRobot toyRobot, Position newPosition) {
        if (table.isPositionOnTable(newPosition)) {
            toyRobot.setPosition(newPosition);
            return CommandResult.success(String.format("The new position is %s", newPosition), this);
        } else {
            return CommandResult.failed(
                    String.format("New position %s will result in the toy robot falling off the table.", newPosition), this);
        }
    }

    private Position calculateNewPosition(Position currentPosition, Direction currentDirection) {
        int newX = currentPosition.getX() + (currentDirection.getPositionModifierX() * unitsToMove);
        int newY = currentPosition.getY() + (currentDirection.getPositionModifierY() * unitsToMove);

        return new Position(newX, newY);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        MoveCommand that = (MoveCommand) other;

        return new EqualsBuilder()
                .append(unitsToMove, that.unitsToMove)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(unitsToMove)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("unitsToMove", unitsToMove)
                .toString();
    }
}