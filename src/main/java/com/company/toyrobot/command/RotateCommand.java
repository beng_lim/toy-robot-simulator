package com.company.toyrobot.command;

import com.company.command.Command;
import com.company.command.CommandResult;
import com.company.domain.Direction;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public abstract class RotateCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(RotateCommand.class);

    @Override
    public CommandResult execute(Table table, ToyRobot toyRobot) {
        if (!table.getToyRobot().isPresent()) {
            return CommandResult.failed("Toy robot has not been placed on table yet.", this);
        }

        Direction newDirection = getDirection(toyRobot);
        log.info("New robot direction will be {}", newDirection);
        toyRobot.setDirection(newDirection);

        return CommandResult.success(String.format("The new direction is %s", newDirection), this);
    }

    private Direction getDirection(ToyRobot toyRobot) {
        switch (getRotation()) {
            case CLOCKWISE:
                return toyRobot.getDirection().rotateClockwise();
            case ANTI_CLOSEWISE:
                return toyRobot.getDirection().rotateAntiClockwise();
            default:
                throw new UnsupportedOperationException("Rotation type not supported: " + getRotation());
        }
    }

    public abstract Rotation getRotation();

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .appendSuper(super.toString())
                .toString();
    }

    enum Rotation {

        CLOCKWISE,
        ANTI_CLOSEWISE
    }
}
