package com.company.toyrobot.command;

import com.company.command.Command;
import com.company.command.CommandResult;
import com.company.domain.Direction;
import com.company.domain.Position;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class PlaceCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(PlaceCommand.class);

    private final Position position;
    private final Direction direction;

    public PlaceCommand(Position position, Direction direction) {
        this.position = position;
        this.direction = direction;
    }

    @Override
    public CommandResult execute(Table table, ToyRobot toyRobot) {
        if (!table.isPositionOnTable(position)) {
            return CommandResult.failed("Position is off the table.", this);
        }

        log.info("Placing robot @ {} with direction of {}", position, direction);
        toyRobot.setPosition(position);
        toyRobot.setDirection(direction);
        table.setToyRobot(toyRobot);
        return CommandResult.success(String.format("The toy robot's position is at %s and direction is %s", position, direction), this);
    }

    public Position getPosition() {
        return position;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        PlaceCommand that = (PlaceCommand) other;

        return new EqualsBuilder()
                .append(position, that.position)
                .append(direction, that.direction)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(position)
                .append(direction)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("position", position)
                .append("direction", direction)
                .toString();
    }
}