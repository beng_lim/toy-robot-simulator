package com.company.toyrobot.command;

import com.company.command.Command;
import com.company.command.CommandResult;
import com.company.table.Table;
import com.company.toyrobot.ToyRobot;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.logging.log4j.util.Strings;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class ReportCommand implements Command {

    private final Writer writer;

    public ReportCommand(Writer writer) {
        this.writer = writer;
    }

    @Override
    public CommandResult execute(Table table, ToyRobot toyRobot) {
        if (!table.getToyRobot().isPresent()) {
            return CommandResult.failed("Toy robot has not been placed on table yet.", this);
        }

        List<String> elements = getElementsToReport(toyRobot);
        return writeElementsToWriter(elements);
    }

    private CommandResult writeElementsToWriter(List<String> elements) {
        try {
            writer.write("Output: " + Strings.join(elements, ',') + System.lineSeparator());
            return CommandResult.success(this);
        } catch (IOException ex) {
            return CommandResult.failed("Could not report position due to IO error.", this, ex);
        }
    }

    private List<String> getElementsToReport(ToyRobot toyRobot) {
        return Arrays.asList(
                String.valueOf(toyRobot.getPosition().getX()),
                String.valueOf(toyRobot.getPosition().getY()),
                toyRobot.getDirection().name()
        );
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("writer", writer)
                .toString();
    }
}