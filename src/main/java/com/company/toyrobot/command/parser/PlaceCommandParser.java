package com.company.toyrobot.command.parser;

import com.company.command.Command;
import com.company.command.reader.parser.CommandInput;
import com.company.command.reader.parser.CommandParser;
import com.company.command.reader.parser.CommandParsingException;
import com.company.domain.Direction;
import com.company.domain.Position;
import com.company.toyrobot.command.PlaceCommand;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PlaceCommandParser implements CommandParser {

    private static final int POSITION_X_INDEX = 0;
    private static final int POSITION_Y_INDEX = 1;
    private static final int DIRECTION_INDEX = 2;
    private static final String COMMAND_NAME = "PLACE";
    private static final String POSITION_X_DISPLAY_NAME = "x";
    private static final String POSITION_Y_DISPLAY_NAME = "y";
    private static final int MINIMUM_NUMBER_OF_ARGUMENTS = 3;

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }

    @Override
    public Command parse(CommandInput commandInput) {
        List<String> arguments = commandInput.getArguments();
        if (arguments.size() < MINIMUM_NUMBER_OF_ARGUMENTS) {
            throw new CommandParsingException("Not enough arguments provided. " +
                    "Expected: " + MINIMUM_NUMBER_OF_ARGUMENTS + " but got: " + arguments.size());
        }
        return new PlaceCommand(parsePositions(arguments), parseDirection(arguments));

    }

    private Position parsePositions(List<String> arguments) {
        String positionXString = arguments.get(POSITION_X_INDEX);
        String positionYString = arguments.get(POSITION_Y_INDEX);

        int positionX = parsePosition(POSITION_X_DISPLAY_NAME, positionXString);
        int positionY = parsePosition(POSITION_Y_DISPLAY_NAME, positionYString);

        return new Position(positionX, positionY);
    }

    private int parsePosition(String positionType, String positionValue) {
        try {
            return Integer.parseInt(positionValue);
        } catch (NumberFormatException ex) {
            throw new CommandParsingException("Invalid position " + positionType + ": " + positionValue, ex);
        }
    }


    private Direction parseDirection(List<String> arguments) {
        String directionString = arguments.get(DIRECTION_INDEX).toUpperCase();
        try {
            return Direction.valueOf(directionString);
        } catch (IllegalArgumentException ex) {
            throw new CommandParsingException("Invalid direction: " + directionString, ex);
        }
    }
}