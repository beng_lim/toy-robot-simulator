package com.company.toyrobot.command.parser;

import com.company.command.Command;
import com.company.command.reader.parser.CommandInput;
import com.company.command.reader.parser.CommandParser;
import com.company.toyrobot.command.LeftCommand;
import org.springframework.stereotype.Component;

@Component
public class LeftCommandParser implements CommandParser {

    private static final String COMMAND_NAME = "LEFT";

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }

    @Override
    public Command parse(CommandInput commandInput) {
        return new LeftCommand();
    }
}