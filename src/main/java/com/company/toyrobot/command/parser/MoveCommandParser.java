package com.company.toyrobot.command.parser;

import com.company.command.Command;
import com.company.command.reader.parser.CommandInput;
import com.company.command.reader.parser.CommandParser;
import com.company.toyrobot.command.MoveCommand;
import org.springframework.stereotype.Component;

@Component
public class MoveCommandParser implements CommandParser {

    private static final String COMMAND_NAME = "MOVE";
    private static final int UNITS_TO_MOVE = 1; // Hard coded for now but can be parsed from input.

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }

    @Override
    public Command parse(CommandInput commandInput) {
        return new MoveCommand(UNITS_TO_MOVE);
    }
}