package com.company.toyrobot.command.parser;

import com.company.command.Command;
import com.company.command.reader.parser.CommandInput;
import com.company.command.reader.parser.CommandParser;
import com.company.toyrobot.command.ReportCommand;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.Writer;

@Component
public class ReportCommandParser implements CommandParser {

    private static final String COMMAND_NAME = "REPORT";

    private Writer outputWriter;

    public ReportCommandParser(@Qualifier("outputWriter") Writer outputWriter) {
        this.outputWriter = outputWriter;
    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }

    @Override
    public Command parse(CommandInput commandInput) {
        return new ReportCommand(outputWriter);
    }
}