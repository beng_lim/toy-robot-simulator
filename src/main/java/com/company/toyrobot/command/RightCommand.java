package com.company.toyrobot.command;

import org.apache.commons.lang3.builder.ToStringBuilder;

import static com.company.toyrobot.command.RotateCommand.Rotation.CLOCKWISE;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class RightCommand extends RotateCommand {

    @Override
    public RotateCommand.Rotation getRotation() {
        return CLOCKWISE;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .toString();
    }
}