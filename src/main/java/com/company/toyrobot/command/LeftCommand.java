package com.company.toyrobot.command;

import org.apache.commons.lang3.builder.ToStringBuilder;

import static com.company.toyrobot.command.RotateCommand.Rotation.ANTI_CLOSEWISE;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class LeftCommand extends RotateCommand {

    @Override
    public Rotation getRotation() {
        return ANTI_CLOSEWISE;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .appendSuper(super.toString())
                .toString();
    }
}