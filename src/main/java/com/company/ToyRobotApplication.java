package com.company;

import com.company.command.CommandResult;
import com.company.command.processor.CommandProcessor;
import com.company.command.reader.CommandReader;
import com.company.command.reader.factory.CommandReaderFactory;
import com.company.domain.ApplicationExecutionContext;
import com.company.table.Table;
import com.company.table.factory.TableFactory;
import com.company.toyrobot.ToyRobot;
import com.company.toyrobot.factory.ToyRobotFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class ToyRobotApplication {

    private static final Logger log = LoggerFactory.getLogger(ToyRobotApplication.class);

    private final CommandReaderFactory commandReaderFactory;
    private final CommandProcessor commandProcessor;
    private final TableFactory tableFactory;
    private final ToyRobotFactory toyFactory;

    public ToyRobotApplication(
            CommandReaderFactory commandReaderFactory,
            CommandProcessor commandProcessor,
            TableFactory tableFactory,
            ToyRobotFactory toyFactory
    ) {
        this.commandReaderFactory = commandReaderFactory;
        this.commandProcessor = commandProcessor;
        this.tableFactory = tableFactory;
        this.toyFactory = toyFactory;
    }

    public static void main(String[] args) {
        SpringApplication.run(ToyRobotApplication.class, args);
    }

    public ApplicationExecutionContext start(String[] args) {
        CommandReader commandReader = commandReaderFactory.fromArguments(args);
        Table table = tableFactory.createDefaultTable();
        ToyRobot toyRobot = toyFactory.createToyRobot();

        List<CommandResult> commandResults = commandReader.read()
                .map(command -> commandProcessor.processCommand(table, toyRobot, command)) // We have a list of command results that we can write out to a file if we wanted.
                .collect(Collectors.toList());

        ApplicationExecutionContext applicationExecutionContext = new ApplicationExecutionContext(table, commandResults);
        log.info("Finished processing. Execution context: {}", applicationExecutionContext);

        return applicationExecutionContext;
    }

    @Bean
    @Profile("!unit-test")
    // Ignore this bean for unit-testing and delegate call to start method to avoid this method running for every new unit test context.
    public CommandLineRunner run() {
        return this::start;
    }
}