# Toy Robot challenge

## Usage

### Running with Gradle

Run the following command:

    ./gradlew -q run --args='<INPUT_FILE_PATH>'
    
To run the application against one of the test files, use the command

    ./gradlew -q run --args='src/test/resources/input_files/sample_a.txt'
    
### Running the JAR

Build the JAR

    ./gradlew clean build
    
Run the following command

    java -Dspring.profiles.active=file -jar build/libs/toy-robot-challenge-1.0-SNAPSHOT.jar <INPUT_FILE_PATH>
    
To run the application against one of the test files, use the command

    java -Dspring.profiles.active=file -jar build/libs/toy-robot-challenge-1.0-SNAPSHOT.jar src/test/resources/input_files/sample_a.txt
    
### Test files

- sample_a.txt
  
- sample_b.txt
  
- sample_c.txt
  
- sample_d.txt
  
- sample_e.txt

- bad_arguments.txt

- bad_commands.txt

- empty.txt
  
- empty_lines.txt

## Design decisions

- As the job description mentions Gradle and Spring Boot, I have used them to demonstrate my knowledge with the tool and framework respectively.

- As there is no command for initialising a table, a table will always be automatically instantiated at the start of the application with the default 5 x 5 values.

- As there is no command for initialising a toy robot, a toy robot will always be automatically instantiated at the start of the application with the the position of (0, 0) and facing NORTH.

- To gain visibility over which commands were not run; any invalid commands or parameters will be logged out to the standard error stream.

- Any failure to parse commands or parameters will stop the execution of the application. As we're building a state machine, it is risky to process any invalid commands as that could completely change the outcome of the application.

- File input is the only implemented input source right now. The application can easily be extended to support other sources by creating a CommandReaderFactory bean and using Spring profiles to switch between input sources.
The allowable input source profiles are [file, std-in] at the moment.

- The factory pattern is used where either:

    1. The created object depends on runtime variables
    2. The lifespan of the object is independent of the application (I.e. ToyRobot and Table)
    3. There could be multiple instances created at runtime, which would then mean the second reason is true too.

- The toy robot was designed to be a dumb/passive robot. I went with keeping the Robot class passive in the sense that it does not know how to move or turn. The incoming commands control the robot's movements. This allowed for better separation of concern and easier testability as I could just mock the Robot object if I wanted to when running unit test for commands.

- To avoid throwing Exceptions, especially for business logic, I created the CommandResult object to store whether the command was successful or not along with a message. As a result, I did not have to manage any Exceptions and can still track errors if they are raised from the commands.
There is an option in the `application.yml` file to control whether the application should fail-fast if there is an invalid command execution.

## Assumptions

The assumptions below allow for a simpler solution to be made without introducing more error scenarios and edge cases.

- There can only be one robot on the table at any given time. 
Any subsequent *PLACE* commands after the first will move the existing toy robot to the new location.
If multiple robots were to be supported, they would have to be given an identifier which would then be provided as part of the input to control individual robots.
Collision would potentially have to be accounted for too.

- The *PLACE* command co-ordinate must be a whole decimal value for the sake of simplicity.
Changes to the field types will allow for decimal values but then we would have to make more assumptions about rounding, precision loss and floating point errors.

- The robot can only be facing one of the following; 0 (NORTH), 90 (EAST), 180 (SOUTH) and 270 (WEST) degrees on the table and nothing in between.

- Thread safety is not a concern as this is a batch application with sequential commands. The ToyRobot and Table classes in their current state are not thread-safe.

## Future improvements

- The command arguments does not allow for spaces and will be ignored. I could follow the CSV RFC4180 standard but it's overkill for now.

## Code coverage

The code coverage can be found in the `code-coverage/` directory. The missing branches are just toString()/equals() and hashCode() calls.

Open the `index.html` to see more.

![Code Coverage](code-coverage/code-coverage.jpg)

NOTE: This wouldn't ordinarily be uploaded to GIT, but it's for your convenience in this challenge :P. 